import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PronosticoHourComponent } from './pronostico-hour.component';

describe('PronosticoHourComponent', () => {
  let component: PronosticoHourComponent;
  let fixture: ComponentFixture<PronosticoHourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PronosticoHourComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PronosticoHourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
