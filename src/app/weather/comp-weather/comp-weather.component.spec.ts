import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompWeatherComponent } from './comp-weather.component';

describe('CompWeatherComponent', () => {
  let component: CompWeatherComponent;
  let fixture: ComponentFixture<CompWeatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompWeatherComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CompWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
