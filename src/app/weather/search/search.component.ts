import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  isOpen!: Boolean;
  city!: string;
  date!: string;
  isDays!: Boolean;

  constructor() { }

  ngOnInit(): void {
    this.isOpen = false;
    this.city = 'London';
    this.isDays = false;
  }

  handleToggle(){
    this.isOpen = !this.isOpen;
  }

  getCity(){
    console.log('hello');
  }

  getDate(){

  }

  handleChangePronost(){
    this.isDays = !this.isDays;
  }

  resetValues(){
    this.city = 'London';
    this.date = '';
  }
}
