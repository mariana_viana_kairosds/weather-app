import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
//import { RouterModule, Routes } from '@angular/router';
import { CompWeatherComponent } from './comp-weather/comp-weather.component';
import { PronosticoHourComponent } from './pronostico-hour/pronostico-hour.component';
import { SearchComponent } from './search/search.component';
import { WeatherUiComponent } from './weather-ui/weather-ui.component';

@NgModule({
  declarations: [
    WeatherUiComponent,
    PronosticoHourComponent,
    SearchComponent,
    CompWeatherComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [WeatherUiComponent, PronosticoHourComponent, SearchComponent, CompWeatherComponent]
})
export class WeatherModule { }
