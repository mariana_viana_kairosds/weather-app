import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RepositoryService {
  constructor(private httpClient: HttpClient) {}

  getAllData(city : string = 'London'): Observable<HttpResponse<any>> {
    return this.httpClient.get<any>(
      `https://api.weatherapi.com/v1/forecast.json?key=e3a59c8eae3d4a7a844103224221005&q=${city}&days=3&alert=yes`,
      { observe: 'response' }
    );
  }

  getDay(city : string = 'London', date: any): Observable<HttpResponse<any>> {
    return this.httpClient.get<any>(
      `https://api.weatherapi.com/v1/forecast.json?key=e3a59c8eae3d4a7a844103224221005&q=${city}&dt=${date}`,
      { observe: 'response' }
    );
  }
}
