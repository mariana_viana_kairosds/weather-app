import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RepositoryService } from '../repository.service';

@Component({
  selector: 'app-weather-ui',
  templateUrl: './weather-ui.component.html',
  styleUrls: ['./weather-ui.component.css']
})
export class WeatherUiComponent implements OnInit {

  proxy$!: Observable<Object>;

  constructor(private service: RepositoryService) { }

  ngOnInit(): void {
    this.proxy$ = this.service.getAllData();
  }

}
